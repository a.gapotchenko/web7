<?php
function auth($db)
{
    if (empty($_SERVER['PHP_AUTH_USER']) ||
        empty($_SERVER['PHP_AUTH_PW'])) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="Admin"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    $request = "SELECT * from admin";
    $result = $db->prepare($request);
    $result->execute();
    $flag = 0;
    while ($data = $result->fetch()) {
        if ($data['login'] == strip_tags($_SERVER['PHP_AUTH_USER']) && password_verify($_SERVER['PHP_AUTH_PW'], $data['pass'])) {
            $flag = 1;
        }
    }
    if ($flag == 0) {
        header('HTTP/1.1 401 Unanthorized');
        header('WWW-Authenticate: Basic realm="Admin"');
        print('<h1>401 Требуется авторизация</h1>');
        exit();
    }
    print('Вы успешно авторизовались.');
}
